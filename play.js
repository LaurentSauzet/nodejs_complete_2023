const person = {
  name: "John",
  age: 30,
  greet() {
    console.log("Hi, I am " + this.name);
  },
};

const copiedPerson = { ...person }
console.log(copiedPerson)

const hobbies = ["Sports", "Cooking", "Accordeon", 50, true];
/* for (let hobby of hobbies) {
  console.log(hobby);
}
console.log(hobbies.map(hobby => {
  return 'Hobby : ' + hobby
}))
console.log(hobbies) */
/* hobbies.push("Battery");
console.log(hobbies); */

const copiedArray = [...hobbies]
console.log(copiedArray)

const toArray = (...args) => {
  return args;
}
console.log(toArray(1, 2, 3, 4))
